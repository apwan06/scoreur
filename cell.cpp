#include "cell.h"
#include <QGraphicsScene>
#include <QPainter>

Cell::Cell(bool *f, quint32 i, quint32 j, QGraphicsScene* sc, QGraphicsItem *parent)
    : QGraphicsObject(parent), alive(f), mi(i), mj(j), isPressed(false)
{
    if(sc!=0) sc->addItem(this);

}

qreal Cell::sidelen = 40;
QRectF Cell::bound = QRectF(-Cell::sidelen/2, -Cell::sidelen/2, Cell::sidelen, Cell::sidelen);
QBrush Cell::deadBrush = QBrush(QColor(15, 15, 15,200));
QBrush Cell::aliveBrush = QBrush(QColor(15, 240, 30, 200));
qreal Cell::len(){
    return sidelen;
}
bool Cell::setLen(qreal l){
    if(l>4 && l<100){
        sidelen = l;
        bound = QRectF(-sidelen/2, -sidelen/2, sidelen, sidelen);
        return true;
    }else
        return false;

}

void Cell::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    if(*alive)
        painter->setBrush(aliveBrush);
    else
        painter->setBrush(deadBrush);
    if(!isPressed)
        painter->setPen(Qt::NoPen);
    painter->drawRect(bound);
}

QRectF Cell::boundingRect() const{
    return bound;
}

void Cell::mousePressEvent(QGraphicsSceneMouseEvent *event){
    isPressed = true;
}

void Cell::mouseReleaseEvent(QGraphicsSceneMouseEvent *event){
    //alive = !alive;
    emit change(mi, mj);
    scene()->update(sceneBoundingRect());
    isPressed = false;
}
