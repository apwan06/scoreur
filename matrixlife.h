#ifndef MATRIXLIFE_H
#define MATRIXLIFE_H
#include <vector>
#include <QDebug>

class MatrixLife{
public:
    MatrixLife(quint32 h, quint32 w):rows(h),cols(w){
        head = new quint8*[rows];
        alive = new bool*[rows];
        for(quint32 i=0;i<rows;++i){
            head[i] = new quint8[cols];
            alive[i] = new bool[cols];
        }
    }
    ~MatrixLife(){
        if(deleteHead())
            qDebug()<<"MatrixBool deleted!";
    }
    bool deleteHead(){
        for(quint32 i=0;i<rows;++i){
            delete []head[i];
            delete []alive[i];
        }
        delete []head; delete []alive;
        return true;
    }

    bool updateNeighbor(){
        quint32 i,j;
        while(! update_i.empty()){
            i = update_i.back();
            j = update_j.back();
            if(alive[i][j]){
                ++head[i-1][j-1];
                ++head[i-1][j];
                ++head[i-1][j+1];
                ++head[i][j-1];
                ++head[i][j+1];
                ++head[i+1][j-1];
                ++head[i+1][j];
                ++head[i+1][j+1];
            }else{
                --head[i-1][j-1];
                --head[i-1][j];
                --head[i-1][j+1];
                --head[i][j-1];
                --head[i][j+1];
                --head[i+1][j-1];
                --head[i+1][j];
                --head[i+1][j+1];
            }
            update_i.pop_back();update_j.pop_back();
        }
        //for four sides
/*        for(quint32 i=1;i<rows;++i){
            quint32 sum = 0;
            if(head[i-1][cols-1]) ++sum;
            if(head[i-1][0]) ++sum;
            if(head[i-1][1]) ++sum;
            if(head[i][cols-1]) ++sum;
            //center
            if(head[i][1]) ++sum;
            if(head[i-1][cols-1]) ++sum;
            if(head[i-1][0]) ++sum;
            if(head[i-1][1]) ++sum;
            if(sum == 3)


                if(head[i][(j+cols-1)%cols]) ++sum;
                if(head[(i+rows-1)%rows][]) ++sum;
                if(head[(i+rows-1)%rows][]) ++sum;
                if(head[i][]) ++sum;
                //self on center
                if(head[i][]) ++sum;
                if(head[(i+1)%rows][]) ++sum;
                if(head[(i+1)%rows][]) ++sum;
                if(head[(i+1)%rows][]) ++sum;
            }
            */
        return true;
    }
    bool *operator()(qint32 x, qint32 y) const{
        while(x<0) x+=cols;
        while(y<0) y+=rows;
        return alive[x%cols]+(y%rows);
    }
    bool **operator()() const{
        return alive;
    }
    bool reset(){//reset head
        for(quint32 i=0;i<rows;++i)
            for(quint32 j=0;j<cols;++j){
                head[i][j] = 0;
                alive[i][j] = false;
            }
        update_i.clear(); update_j.clear();
        return true;
    }
    bool set(quint32 i, quint32 j, bool f = false){
        if(i==0 || j==0 || i==rows-1 || j==cols-1)
            return false;

        if(f){
            update_i.push_back(i);
            update_j.push_back(j);
            alive[i][j] = !alive[i][j];
        }else{//already changed alive[i][j]
            update_i.push_back(i);
            update_j.push_back(j);
        }
        return true;

    }

    bool rebuild(quint32 h, quint32 w){
        if(rows == h && cols == w){
            reset();
            return false;
        }else{
            deleteHead();
            rows = h; cols = w;
            head = new quint8*[rows];
            for(quint32 i=0;i<rows;++i){
                head[i] = new quint8[cols];
            }
            return true;
        }
    }

private:
    quint32 rows, cols;
    quint8 **head;
    bool **alive;
    std::vector<quint32> update_i;
    std::vector<quint32> update_j;
public:
    void updateLife(){//update alive not on sides
        //update_i.clear(); update_j.clear();
        for(quint32 i=1;i<rows-1;++i){
            for(quint32 j=1;j<cols-1;++j){
                if(alive[i][j]==false && head[i][j]==3){
                    update_i.push_back(i);
                    update_j.push_back(j);
                    alive[i][j] = true;
                }
                if(alive[i][j]==true && (head[i][j]<2 || head[i][j]>3)){
                    update_i.push_back(i);
                    update_j.push_back(j);
                    alive[i][j] = false;

                }
            }
        }
    }

};

#endif // MATRIXLIFE_H
