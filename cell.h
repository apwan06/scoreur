#ifndef CELL_H
#define CELL_H
#include <QGraphicsObject>
#include <QGraphicsSceneMouseEvent>
class QGraphicsScene;
class Cell:public QGraphicsObject
{
    Q_OBJECT
public:
    Cell(bool *f, quint32 i, quint32 j, QGraphicsScene *sc=0, QGraphicsItem *parent=0);

    friend class QGraphicsScene;
    static qreal len();
    static bool setLen(qreal l);

signals:
    void change(quint32, quint32);


protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    bool *alive;
    bool isPressed;
    quint32 mi, mj;
    static qreal sidelen;
    static QRectF bound;
    static QBrush deadBrush, aliveBrush;
};

#endif // CELL_H
