#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include "matrixlife.h"
#include "cell.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    quint16 numx = 200, numy=100;
    Cell::setLen(5);
    qreal intv = Cell::len()+1;
    lifeScene = new QGraphicsScene(QRectF(ui->lifeView->pos(), QSizeF(intv*numx, intv*numy)), this);
    ui->lifeView->setScene(lifeScene);
    mb = new MatrixLife(numy,numx);
    mb->reset();

    Cell *cell;
    for(quint32 i=0; i<numy; ++i)
        for(quint32 j=0; j<numx;++j){
            cell = new Cell((*mb)(i,j), i, j, lifeScene);
            cell->setPos((j+0.5)*intv, (i+0.5)*intv);
            connect(cell, SIGNAL(change(quint32,quint32)), SLOT(change(quint32,quint32)));
        }
    timer = new QTimer(this);
    timer->setInterval(500);
    timer->start();
}

MainWindow::~MainWindow()
{
    delete mb;
    delete ui;
}

void MainWindow::change(quint32 i, quint32 j){
    mb->set(i,j,true);
    mb->updateNeighbor();
    qDebug()<<i<<j;
}

void MainWindow::on_pushButton_clicked()
{
    mb->updateLife();
    mb->updateNeighbor();
    lifeScene->update(lifeScene->sceneRect());
}

void MainWindow::on_btnauto_clicked()
{
    if(ui->btnauto->text()==QString("Start")){
        ui->btnauto->setText(QString("Stop"));
        connect(timer, SIGNAL(timeout()), SLOT(on_pushButton_clicked()));
    }else{
        ui->btnauto->setText(QString("Start"));
        disconnect(timer, SIGNAL(timeout()), this, SLOT(on_pushButton_clicked()));
    }
}
