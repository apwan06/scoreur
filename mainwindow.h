#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QTimer>

class QGraphicsScene;
namespace Ui {
class MainWindow;
}

class MatrixLife;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void change(quint32, quint32);
private slots:
    void on_pushButton_clicked();

    void on_btnauto_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *lifeScene;
    MatrixLife *mb;
    QTimer *timer;
};

#endif // MAINWINDOW_H
