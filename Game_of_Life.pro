#-------------------------------------------------
#
# Project created by QtCreator 2014-09-05T07:25:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Game_of_Life
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cell.cpp

HEADERS  += mainwindow.h \
    cell.h \
    matrixlife.h

FORMS    += mainwindow.ui
